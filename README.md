![](.README_images/background.png)

# Back-End Documentation for Our Second Hand Market Application :smiling_imp:

## Demo Video

Check our demo video on YouTube: https://youtu.be/0pUVFvVXTAM!

## Architectural Design

![](.README_images/architecture.png)

## Entity Relation Diagram

Database Schema in MySQL: [Second Hand Market Application Database Schema Design](docs/Database.md#database-schema-design)

![](.README_images/erd.png)

Check out the updated ER Diagram: <https://app.lucidchart.com/documents/view/42b49347-7318-4d8a-badb-5c69cd6a9cc7>

## RESTful API Endpoints

### Home URL:

The backend service is deployed on the Amazon Web Service using Elastic Beanstalk,
the URL is: <http://zarathos-env.eba-ccwytkkn.us-east-2.elasticbeanstalk.com/>.

### User Service

- [`/users`](docs/api/Users.md#get-users) :sunglasses:
    - Method Type: `GET`
    - Request Parameters: `id_token`
    - Response: all users' profiles stored in the backend database, authorized user's access only.
- [`/users/user`](docs/api/Users.md#get-usersuser) :sunglasses:
    - Method Type: `GET`
    - Request Parameters: `id_token`
    - Response: user profile with specified `userUID`, authorized user's access only.
- [`/users`](docs/api/Users.md#post-users) :sunglasses:
    - Method Type: `POST`
    - Request Parameters: `id_token`
    - Response: saved user object with given `id_token`
- [`/users/user/products`](docs/api/Users.md#get-usersuserproducts) :sunglasses:
    - Method Type: `GET`
    - Request Parameters: `id_token`
    - Response: user profile with specified `userUID`, authorized user's access only.

### Product Service

- [`/index`](docs/api/Products.md#get-index) :sunglasses:
    - Method Type: `GET`
    - Request Parameters: None
    - Response: product list ordered by categories for home page display
- [`/products`](docs/api/Products.md#get-products) :sunglasses:
    - Method Type: `GET`
    - Request Parameters: `id_token`, `page`, `page_size`
    - Response: all products in the database paginated, accessible for all registered users
- [`/products/nearby`](docs/api/Products.md#get-productsnearby) :sunglasses:
    - Method Type: `GET`
    - Request Parameters: `lat`, `lon`,`id_token`
    - Response: all products nearby (lat/lon +/- 0.25) with given GPS coordinates, registered user access only
- [`/products/product/{productId}`](docs/api/Products.md#get-productsproductproductid) :sunglasses:
    - Method Type: `GET`
    - Request Parameters: `id_token`
    - Response: product with given productId
- [`/products/search`](docs/api/Products.md#get-productssearch) :sunglasses:
    - Method Type: `GET`
    - Request Parameters: `id_token`, `keywords`, `category`, `state`
    - Response: products with specified search query, request parameters can be used in 
        arbitrary combination as long as the `id_token` is provided
- [`/products`](docs/api/Products.md#post-products) :sunglasses:
    - Method Type: `POST`
    - Request Parameters: `id_token`
    - Request Body: JSON Object representing `Product` body
    - Response: saved product object with given `id_token` and request body

### Favorite Service

- [`/users/user/favorites`](docs/api/Favorites.md#get-usersuserfavorites) :sunglasses:
    - Method Type: `GET`
    - Request Parameters: `id_token`
    - Response: favorite items for user with matching `userUID`
- [`/users/user/favorites`](docs/api/Favorites.md#post-usersuserfavorites) :sunglasses:
    - Method Type: `POST`
    - Request Parameters: `id_token`, `productId`
    - Response: saved `favorite` object with given user and product specified by `id_token` and `productId`
- [`/users/user/favorites`](docs/api/Favorites.md#delete-usersuserfavorites) :sunglasses:
    - Method Type: `DELETE`
    - Request Parameters: `id_token`, `productId`
    - Response: "OK" upon successful deletion of the `favorite` object with given user and product specified by `id_token` and `productId`
    
### Request Service

- [`/users/customer/requests`](docs/api/Requests.md#get-userscustomerrequests)
    - Method Type: `GET`
    - Request Parameters: `id_token`
    - Response: request items made by the customer corresponding to the `id_token`
- [`/users/customer/requests`](docs/api/Requests.md#post-userscustomerrequests)
    - Method Type: `POST`
    - Request Parameters: `id_token`, `product_id`
    - Response: created request item made by the customer corresponding to the `id_token` with the product of `product_id`
- [`/users/customer/requests/request/{requestId}`](docs/api/Requests.md#delete-userscustomerrequestsrequestrequestid)
    - Method Type: `DELETE`
    - Request Parameters: `id_token`
    - Response: message showing success of request cancellation by the customer
- [`/users/seller/requests`](docs/api/Requests.md#get-userssellerrequests)
    - Method Type: `GET`
    - Request Parameters: `id_token`
    - Response: request items of the products belonging to the seller corresponding to the `id_token`
- [`/users/seller/requests/request/{requestId}`](docs/api/Requests.md#put-userssellerrequestsrequestrequestid)
    - Method Type: `PUT`
    - Request Parameters: `id_token`
    - Response: created confirmed order item upon request confirmation
- [`/users/seller/requests/request/{requestId}`](docs/api/Requests.md#delete-userssellerrequestsrequestrequestid)
    - Method Type: `DELETE`
    - Request Parameters: `id_token`
    - Response: message showing success of request declination by the seller
    
### Order Service

- [`/orders/seller`](docs/api/Orders.md#get-ordersseller)
    - Method Type: `GET`
    - Request Parameters: `id_token`
    - Response: order items for the seller with `userUID` corresponds to `id_token`
- [`/orders/customer`](docs/api/Orders.md#get-orderscustomer) :sunglasses:
    - Method Type: `GET`
    - Request Parameters: `id_token`
    - Response: order items for the customer with `userUID` corresponds to `id_token`
- [`/orders/seller`](docs/api/Orders.md#post-ordersseller)
    - Method Type: `POST`
    - Request Parameters: `id_token`, `customer_id`, `product_id`
    - Response: created order item with `id_token`, `customer_id`, and `product_id`
- [`/orders/seller/order/{order_id}`](docs/api/Orders.md#put-orderssellerorderorder_id)
    - Method Type: `PUT`
    - Request Parameters: `id_token`
    - Response: confirmed order item with `order_id`, accessible for `seller` only
- [`/orders/customer/order/{order_id}`](docs/api/Orders.md#put-orderscustomerorderorder_id)
    - Method Type: `PUT`
    - Request Parameters: `id_token`, `rating`
    - Response: rated order item with `order_id`, accessible for `customer` only and available for only one-time use 
        for each customer
- [`/orders/seller/order/{order_id}`](docs/api/Orders.md#delete-orderssellerorderorder_id)
    - Method Type: `DELETE`
    - Request Parameters: `id_token`
    - Response: "OK" upon successful deletion of the `productOrder` object with given user and product order item 
        specified by `id_token` and `order_id`

## Cloud and Database Settings

### Host URL:

The backend service is deployed on the Amazon Web Service using Elastic Beanstalk,
the URL is: <http://zarathos-env.eba-ccwytkkn.us-east-2.elasticbeanstalk.com/>.

### Amazon RDS DB Instance

You are always welcome to connect with the backend database for testing. The configuration of the
database can be seen below:

**NOTE**

Currently we are not using Amazon RDS and data can only be accessed with our in-memory db because the 
generated test data is **BIG**.

> ***AWS RDS DB Instance: Second Hand Market***
> 
> Host URL: zarathosdb-instance.cdmo8xowcine.us-east-2.rds.amazonaws.com
>
> Port: 3306
>
> Username: admin, Password: ask xin
>
> DB Name: shmktDB

Note this database can also be accessed via H2-Console with host url and user information, here the db type should be MySQL.

### In-Memory H2-DB Set-Up (Optional)

There's an embedded h2 database for testing purposes. To access the DB, go to path `/h2-console`,
the default username is sa, and password is *ask xin*.

**Login Information for In-Memory Database**

URL: <http://zarathos-env.eba-ccwytkkn.us-east-2.elasticbeanstalk.com/h2-console>

> Saved Settings: Generic H2 (Embedded)
> Setting Name: Generic H2 (Embedded)
> Driver Class: org.h2.Driver
> JDBC URL: jdbc:h2:mem:ecommerce
> User Name: sa
> Password: {ask xin}

![](.README_images/h2login.png)

**Demo**

Click on the table name it'll automatically generate the SQL Command for selecting all the data in the table for you.
The default max rows for query display is 1000, and can be customized in the `Max rows` section.

![](.README_images/h2demo.png)


